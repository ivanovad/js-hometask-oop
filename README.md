# js-hometask-oop

## Ответьте на вопросы

### Откуда в объектах методы `toString` и `valueOf`, хотя вы их не добавляли?
> Ответ: Это методы прототипа (родителя) всех объектов Object, к которым могут обратиться дочерние (т.е. в данном случае любые) объекты. 

### Чем отличаются `__proto__` и `prototype`? 
> Ответ: __proto__ это свойство экземляра дочернего объекта, связывающее его с объектом-прототипом (родителем).
> prototype же является свойством конструктора, а не экземпляра.

### Что такое `super` в классах?
> Ответ: Данное ключевое слово даёт доступ к методам родителя. Для дочернего класса обязательно следует явно вызывать в своём конструкторе конструктор родителя, это можно сделать указав super() в конструкторе потомка.

### Что такое статический метод класса, как его создать? 
> Ответ: это метод, не относящийся к конктерному экземпляру объекта, а ко всему классу в целом. Чтобы его создать, необходимо при создании класса около метода написать ключевое слово static.


## Выполните задания

* Установите зависимости `npm install`;
* Допишите функции в `tasks.js`;
* Проверяте себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.
